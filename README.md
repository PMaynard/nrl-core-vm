# NRL Core Virtual Machine

Requirements [packer](https://www.packer.io/) and virtual box.

	packer build nrl-core.json

# Dev 

	sudo mount -t vboxsf nrl-core-vm tmp
	ansible-playbook -i "localhost," -c local playbook.yml


WindowKey+Left Mouse 	Move floating window
WindowKey+Right Mouse 	Resize floating window
Ctrl+Shift+c 			Close Window
Ctrl+d 					Type process name and hit enter e.g. 'core-gui' to start NRL Core
Ctrl+enter				Open a Terminal
WindowsKey+number 		Move to workspace
WindowsKey+Shift+number 		Move window to workspace


File->Open->lab-3.imn (/home/core/.core/configs)
Press the green cirucle with white triangle.
Wait for 30s for the networks to initiate.
Double click on any machine to open a terminal. enter commands. Ctrl+D or type exit to close.
Right click on any node to start packet capture from either tcpdump, tshark, and wireshark.
You can also start and stop services running on the nodes.

athing - 10.0.6.10 - HTTPd
bthing - 10.0.6.11 - ftp
cthing - 10.0.6.12 - nothing 
dthing - 10.0.6.13 - sshd

company-a (10.0.0.10) and company-b (10.0.8.10) both have SSHd running.

